/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 17:32:51 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/15 18:03:30 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void		*ft_memcpy(void *s1, const void *s2, size_t n)
{
	char	*dest;
	char	*str;
	size_t	i;

	i = 0;
	dest = (char *)s1;
	str = (char *)s2;
	while (i < n)
	{
		dest[i] = str[i];
		i++;
	}
	return ((void *)dest);
}
