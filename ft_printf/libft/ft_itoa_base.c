/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 22:08:15 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/17 10:34:44 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"
#define BASE "0123456789abcdef"

static char	*get_num(long long num, int base)
{
	char	*s;
	int		i;

	s = ft_strnew(20);
	i = 0;
	while (num > 0)
	{
		s[i++] = BASE[num % base];
		num /= base;
	}
	s[i] = '\0';
	ft_strrev(s);
	return (s);
}

char		*ft_itoa_base(int num, int base)
{
	char	*zero;

	if (base < 2 || base > 16)
		return (NULL);
	if (num == 0)
	{
		zero = ft_strnew(1);
		zero[1] = '\0';
		return (zero);
	}
	else if (base == 10)
		return (ft_itoa(num));
	else if (num < 0)
		return (get_num(((long long)num * -1), base));
	else
		return (get_num((long long)num, base));
}
