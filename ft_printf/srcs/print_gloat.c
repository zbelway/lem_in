/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_gloat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/20 21:19:54 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:50:46 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				convert_gloat(long long i, long double d,
		int p, t_attribute *att)
{
	long long int	n;
	char			*s1;
	char			*s2;

	n = 1;
	s1 = ft_itoa(i);
	s2 = ft_strnew(p);
	att->precision = p;
	s2[p] = '\0';
	while (p-- > 0)
		n *= 10;
	n = (n * d + .5);
	while (n % 10 == 0)
		n /= 10;
	s2 = my_strcpy(s2, ft_itoa(n));
	if (n > 0)
		combine_num_parts(s1, s2, att);
	else
		put_num(i, att);
	free(s1);
	free(s2);
}

void				e_itoa(long double d, int e, int p, t_attribute *att)
{
	char			*s;
	int				i;
	int				n;

	i = 1;
	s = ft_strnew(30);
	n = d;
	s[0] = n + '0';
	if (p)
		s[i++] = '.';
	while (p)
	{
		d -= n;
		d *= 10;
		n = d + .5;
		s[i++] = n + '0';
		p--;
	}
	s[i++] = 'e';
	s[i++] = (e >= 0) ? '+' : '-';
	s[i++] = '0';
	s[i] = '\0';
	s = ft_strcat(s, ft_itoa(ft_absval(e)));
	put_float(s, att);
}

void				convert_eloat(long double d, int p, t_attribute *att)
{
	int				e;

	e = 0;
	if (d >= 1)
	{
		while (d > 9)
		{
			e++;
			d /= 10;
		}
		e_itoa(d, e, p, att);
	}
	else
	{
		while (d < 1)
		{
			e--;
			d *= 10;
		}
		e_itoa(d, e, p, att);
	}
}

void				pr_eloat(va_list args, t_attribute *att)
{
	long double		d;

	if (att->length == 'L')
		d = va_arg(args, long double);
	else
		d = va_arg(args, double);
	if (d < 0)
	{
		att->neg = 1;
		d *= 1;
	}
	if (!att->is_precision)
		convert_eloat(d, 6, att);
	else
		convert_eloat(d, att->precision, att);
}

void				pr_gloat(va_list args, t_attribute *att)
{
	long double		d;
	long long		i;

	if (att->length == 'L')
		d = va_arg(args, long double);
	else
		d = va_arg(args, double);
	if (d < 0)
	{
		att->neg = 1;
		d *= -1;
	}
	i = d;
	d -= i;
	if (d == 0)
		put_num(i, att);
	else if (att->is_precision && att->precision == 0)
		convert_gloat(i, d, 1, att);
	else if (!att->is_precision)
		convert_gloat(i, d, 6, att);
	else
		convert_gloat(i, d, att->precision, att);
}
