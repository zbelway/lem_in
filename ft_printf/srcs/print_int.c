/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_int.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/06 17:35:51 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 17:53:52 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

intmax_t		unsigned_int(va_list args, t_attribute *att)
{
	if (att->length == 'H')
		return ((unsigned char)va_arg(args, int));
	else if (att->length == 'h')
		return ((unsigned short int)va_arg(args, int));
	else if (att->length == 'l')
		return (va_arg(args, unsigned long int));
	else if (att->length == 'L')
		return (va_arg(args, unsigned long long int));
	else if (att->length == 'j')
		return (va_arg(args, unsigned long long));
	else if (att->length == 'z')
		return (va_arg(args, size_t));
	else if (att->length == 't')
		return ((unsigned)va_arg(args, ptrdiff_t));
	else if (att->length == 'q')
		return (va_arg(args, t_uquad));
	else
		return (va_arg(args, unsigned int));
}

intmax_t		signed_int(va_list args, t_attribute *att)
{
	if (att->length == 'H')
		return ((signed char)va_arg(args, int));
	else if (att->length == 'h')
		return ((short int)va_arg(args, int));
	else if (att->length == 'l')
		return (va_arg(args, long int));
	else if (att->length == 'L')
		return (va_arg(args, long long int));
	else if (att->length == 'j')
		return (va_arg(args, intmax_t));
	else if (att->length == 'z')
		return (va_arg(args, size_t));
	else if (att->length == 't')
		return (va_arg(args, ptrdiff_t));
	else if (att->length == 'q')
		return (va_arg(args, t_quad));
	else
		return (va_arg(args, int));
}

void			pr_int(va_list args, t_attribute *att)
{
	if (att->spec == 'd' || att->spec == 'i')
		put_num(signed_int(args, att), att);
	else if (att->spec == 'D')
		put_num(va_arg(args, long int), att);
}
