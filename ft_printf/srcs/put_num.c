/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_num.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 02:35:32 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/21 02:52:15 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				fill_space(t_attribute *att, unsigned int n)
{
	unsigned int	i;

	i = 0;
	while (i < n)
	{
		if (att->precision == 0 && att->pad_zero)
			ft_putchar('0');
		else
			ft_putchar_fd(' ', att->fd);
		att->written++;
		i++;
	}
}

void				iteri_intmax(intmax_t n, int i, t_attribute *att)
{
	if ((n >= 10 || n <= -10) || i > 1)
		iteri_intmax(n / 10, i - 1, att);
	ft_putchar_fd(ft_absval(n % 10) + '0', att->fd);
}

unsigned int		put_intmax(intmax_t n, t_attribute *att, unsigned int i)
{
	if (!att->neg && att->show_sign)
		i++;
	if ((att->left_adjustment || att->pad_zero) && att->show_sign && !att->neg)
		ft_putchar_fd('+', att->fd);
	else if (!att->neg && att->begin_blank)
		if ((att->width <= i || att->left_adjustment || att->pad_zero) && i++)
			ft_putchar_fd(' ', att->fd);
	if (att->pad_zero && att->neg)
		ft_putchar_fd('-', att->fd);
	if (att->width > i && !att->left_adjustment)
		fill_space(att, (att->width - i));
	if (!att->neg && att->show_sign && !att->left_adjustment && !att->pad_zero)
		ft_putchar_fd('+', att->fd);
	if (!att->pad_zero && att->neg)
		ft_putchar_fd('-', att->fd);
	if (att->p)
		iteri_intmax(n, att->precision, att);
	if (att->width > i && att->left_adjustment)
		fill_space(att, (att->width - i));
	return (i);
}

void				put_num(intmax_t n, t_attribute *att)
{
	unsigned int	i;
	intmax_t		num;

	if (n != 0)
		att->p = '.';
	i = 1;
	if (!att->p)
		i = 0;
	if (n < 0)
		att->neg = '-';
	num = n;
	while (n > 9 || n < -9)
	{
		n /= 10;
		i++;
	}
	if (att->precision > i)
		i = att->precision;
	if (att->neg)
		i++;
	i = put_intmax(num, att, i);
	att->written += i;
}
