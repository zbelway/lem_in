/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_char.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 07:23:04 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/21 02:51:43 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned int		wchar_len(wchar_t c)
{
	if (c <= 0x7F)
		return (1);
	else if (c <= 0x7FF)
		return (2);
	else if (c <= 0xFFFF)
		return (3);
	else if (c <= 0x10FFFF)
		return (4);
	return (0);
}

void				pr_wchar(t_attribute *att, wchar_t c)
{
	unsigned int	i;

	i = 0;
	if (att->width && att->precision && att->precision < att->width)
		i = att->width - att->precision;
	else if (att->width && att->precision)
		i = 0;
	else if (att->width)
		i = att->width - 1;
	if (att->left_adjustment)
	{
		ft_putwchar_fd(c, att->fd);
		fill_space(att, i);
	}
	else
	{
		fill_space(att, i);
		ft_putwchar_fd(c, att->fd);
	}
	att->written += wchar_len(c);
}

void				print_wide_char(wchar_t c, t_attribute *att)
{
	if (!c)
	{
		if (att->width)
			fill_space(att, (att->width - 1));
		ft_putchar_fd('\0', att->fd);
		att->written++;
	}
	else
		pr_wchar(att, c);
}

void				pr_char(va_list args, t_attribute *att)
{
	unsigned char	c;

	if (att->length == 'l' || att->spec == 'C')
		print_wide_char(va_arg(args, wint_t), att);
	else
	{
		c = (unsigned char)va_arg(args, int);
		if (!c)
			c = '\0';
		pr_percent(att, c);
	}
}
