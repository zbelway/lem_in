/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_str.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 19:43:23 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 14:53:53 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				put_str_no_width(char *s, t_attribute *att)
{
	if (att->is_precision)
	{
		ft_putnstr_fd(s, att->precision, att->fd);
		if (att->precision > ft_strlen(s))
			att->written += ft_strlen(s);
		else
			att->written += att->precision;
	}
	else
	{
		ft_putstr_fd(s, att->fd);
		att->written += ft_strlen(s);
	}
}

void				put_wstr_no_width(wchar_t *s, t_attribute *att)
{
	if (att->is_precision)
		att->written += my_putnwstr(s, att->precision, att->fd, 1);
	else
	{
		ft_putwstr_fd(s, att->fd);
		att->written += ft_wstrlen(s);
	}
}

void				put_str(char *s, t_attribute *att)
{
	if (att->width && att->is_precision && att->width > att->precision)
	{
		if (att->left_adjustment)
			ft_putnstr_fd(s, att->precision, att->fd);
		if (att->precision > ft_strlen(s))
			fill_space(att, (att->width - ft_strlen(s)));
		else
			fill_space(att, (att->width - att->precision));
		if (!att->left_adjustment)
			ft_putnstr_fd(s, att->precision, att->fd);
		att->written += att->precision < ft_strlen(s) ?
			att->precision : ft_strlen(s);
	}
	else if (att->width && att->width > ft_strlen(s))
	{
		if (att->left_adjustment)
			ft_putstr_fd(s, att->fd);
		fill_space(att, (att->width - ft_strlen(s)));
		if (!att->left_adjustment)
			ft_putstr_fd(s, att->fd);
		att->written += ft_strlen(s);
	}
	else
		put_str_no_width(s, att);
}

void				put_wstr(wchar_t *s, t_attribute *att)
{
	if (att->width && att->is_precision && att->width > att->precision)
	{
		if (att->left_adjustment)
			att->written += my_putnwstr(s, att->precision, att->fd, 1);
		if (att->precision > ft_wstrlen(s))
			fill_space(att, (att->width - ft_wstrlen(s)));
		else
			fill_space(att, (att->width -
						my_putnwstr(s, att->precision, att->fd, 0)));
		if (!att->left_adjustment)
			att->written += my_putnwstr(s, att->precision, att->fd, 1);
	}
	else if (att->width && att->width > ft_wstrlen(s))
	{
		if (att->left_adjustment)
			ft_putwstr_fd(s, att->fd);
		fill_space(att, (att->width - ft_wstrlen(s)));
		if (!att->left_adjustment)
			ft_putwstr_fd(s, att->fd);
		att->written += ft_wstrlen(s);
	}
	else
		put_wstr_no_width(s, att);
}

void				pr_str(va_list args, t_attribute *att)
{
	char			*str;
	wchar_t			*strw;

	if (att->length == 'l' || att->spec == 'S')
	{
		strw = va_arg(args, wchar_t*);
		if (!strw && !att->is_precision)
			str = ft_strdup("(null)");
		if (!strw && !att->is_precision)
			put_str(str, att);
		if (strw)
			put_wstr(strw, att);
		else if (!strw && att->is_precision)
			put_str("", att);
	}
	else
	{
		str = va_arg(args, char*);
		if (!str && !att->is_precision)
			str = ft_strdup("(null)");
		if (str)
			put_str(str, att);
		else
			put_str("", att);
	}
}
