/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_putnwstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 16:03:38 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/20 20:02:18 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned int		my_putnwstr(wchar_t *s, size_t len, int fd, int p)
{
	size_t			i;
	unsigned int	ret;

	i = 0;
	ret = 0;
	while (*s && i < len)
	{
		if (*s <= 0x7F)
			i++;
		else if (*s <= 0x7FF)
			i += 2;
		else if (*s <= 0xFFFF)
			i += 3;
		else if (*s <= 0x10FFFF)
			i += 4;
		if (i <= len)
		{
			if (p)
				ft_putwchar_fd(*s++, fd);
			ret = i;
		}
	}
	return (ret);
}
