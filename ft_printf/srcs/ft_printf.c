/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 15:35:59 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 17:59:14 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void static		handle_args(va_list args, t_attribute *att, char c)
{
	if (c == '%')
		pr_percent(att, '%');
	else if (c == 's' || c == 'S')
		pr_str(args, att);
	else if (c == 'd' || c == 'D' || c == 'i')
		pr_int(args, att);
	else if (c == 'o' || c == 'O' || c == 'u' || c == 'U')
		pr_hex(args, att);
	else if (c == 'x' || c == 'X' || c == 'b' || c == 'B')
		pr_hex(args, att);
	else if (c == 'g' || c == 'G')
		pr_gloat(args, att);
	else if (c == 'e' || c == 'E')
		pr_eloat(args, att);
	else if (c == 'c' || c == 'C')
		pr_char(args, att);
	else if (c == 'f' || c == 'F')
		pr_float(args, att);
	else if (c == 'p')
		pr_pointer(args, att);
	else if (c == 'n')
		pr_written(args, att);
	else if (c == '!')
		att->fd = va_arg(args, int);
}

void static		after_percent(const char **format, va_list args,
		t_attribute *att, int fd)
{
	att->fd = fd;
	att->p = '.';
	if (**format == '{')
		if (parse_color(format, att))
			return ;
	parse_flags(format, att);
	parse_width(format, args, att);
	parse_precision(format, args, att, 0);
	parse_length(format, att);
	parse_spec(format, att);
	if (att->spec)
		handle_args(args, att, att->spec);
}

size_t static	print_format(const char *format, va_list args, int fd)
{
	char		*rest;
	t_attribute	att;
	int			written;

	written = 0;
	rest = ft_strchr(format, '%');
	while (rest)
	{
		ft_bzero(&att, sizeof(att));
		ft_putnstr_fd(format, (rest - format), fd);
		att.written += (rest - format);
		format = rest + 1;
		att.n = written + att.written;
		after_percent(&format, args, &att, fd);
		fd = att.fd;
		rest = ft_strchr(format, '%');
		written += att.written;
	}
	if (!rest)
	{
		ft_putstr_fd(format, fd);
		written += ft_strlen(format);
	}
	return (written);
}

int				ft_printf(const char *format, ...)
{
	va_list		args;
	size_t		ret;

	ret = 0;
	if (format)
	{
		va_start(args, format);
		ret = print_format(format, args, 1);
		va_end(args);
	}
	return (ret);
}
