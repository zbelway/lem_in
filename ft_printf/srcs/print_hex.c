/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/10 11:03:56 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:50:59 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned int		put_pound(intmax_t n, t_attribute *att, int p)
{
	if ((n != 0 && (att->spec == 'x' || att->spec == 'X')) || att->spec == 'p')
	{
		if (p)
		{
			if (att->spec == 'X')
				write(att->fd, "0X", 2);
			else
				write(att->fd, "0x", 2);
		}
		return (2);
	}
	else if ((n != 0 || att->is_precision)
			&& (att->spec == 'o' || att->spec == 'O'))
	{
		if (p)
			ft_putchar_fd('0', att->fd);
		return (1);
	}
	return (0);
}

void				put_hex_width(intmax_t n, unsigned int i,
		char *s, t_attribute *att)
{
	if (att->left_adjustment)
	{
		if (att->pound && (att->spec != 'o' || *s != '0' || !att->p))
			put_pound(n, att, 1);
		if (att->p)
			ft_putstr_fd(s, att->fd);
		if (att->width > i)
			fill_space(att, (att->width - i));
	}
	else
	{
		if (att->pound && att->pad_zero
				&& (att->spec != 'o' || *s != '0' || !att->p))
			put_pound(n, att, 1);
		if (att->width > i)
			fill_space(att, (att->width - i));
		if (att->pound && !att->pad_zero
				&& (att->spec != 'o' || *s != '0' || !att->p))
			put_pound(n, att, 1);
		if (att->p)
			ft_putstr_fd(s, att->fd);
	}
	ft_strdel(&s);
}

void				pr_hex(va_list args, t_attribute *att)
{
	uintmax_t		n;
	char			*s;
	unsigned int	i;

	n = 0;
	i = 0;
	if (att->spec == 'O' || att->spec == 'U')
		n = va_arg(args, unsigned long int);
	else
		n = unsigned_int(args, att);
	if (n > 0)
		att->p = '.';
	if (att->spec == 'B')
		att->precision = 8;
	s = ft_uitoa_base_mod(n, att);
	if (att->pound && (att->spec != 'o' || *s != '0' || !att->p))
		i += put_pound(n, att, 0);
	if (att->p)
		i += ft_strlen(s);
	put_hex_width(n, i, s, att);
	att->written += i;
}

void				pr_pointer(va_list args, t_attribute *att)
{
	att->length = 'l';
	att->pound = '#';
	pr_hex(args, att);
}

void				pr_written(va_list args, t_attribute *att)
{
	intmax_t		*p;

	if (att->length == 'H')
		p = (intmax_t *)va_arg(args, signed char *);
	else if (att->length == 'h')
		p = (intmax_t *)va_arg(args, short *);
	else if (att->length == 'l')
		p = va_arg(args, long *);
	else if (att->length == 'L')
		p = (intmax_t *)va_arg(args, long long *);
	else if (att->length == 'j')
		p = va_arg(args, intmax_t *);
	else if (att->length == 't')
		p = va_arg(args, ptrdiff_t *);
	else if (att->length == 'z')
		p = (intmax_t *)va_arg(args, size_t *);
	else if (att->length == 'q')
		p = (intmax_t *)va_arg(args, t_quad *);
	else
		p = (intmax_t *)va_arg(args, int *);
	*p = att->n;
}
