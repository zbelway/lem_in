/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/15 23:00:07 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/05 12:58:43 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include <libft.h>
# include <ft_printf.h>

typedef struct		s_rooms
{
	char			*room;
	int				room_num;
	int				x;
	int				y;
	int				distance;
	int				*connections;
	int				connects_to;
	struct s_rooms	*next;
	struct s_rooms	*prev;
}					t_rooms;

typedef struct		s_ant
{
	t_rooms			*farm;
	t_rooms			*first;
	int				**path;
	int				paths;
	int				rooms;
	int				ants;
	int				x;
	int				y;
	int				start;
	int				end;
	char			*buf;
	char			t;
	char			p;
	char			c;
	char			v;
	char			e;
}					t_ant;

void				error(t_ant *ant, char *line);
void				color_code(int i);
void				print_path(t_ant *ant, int **path, int i);
void				goto_room_num(t_ant *ant, int room_num);
void				input_number(t_ant *ant, char *line);
void				input_command(t_ant *ant, char *line);
int					is_connection(t_ant *ant, char *line);
void				create_connection(t_ant *ant, char *line);
void				create_room(t_ant *ant, char *room, int x, int y);
void				set_up_map(t_ant *ant);
void				path_distance(t_ant *ant);
void				create_paths(t_ant *ant);
void				first_path(t_ant *ant);
void				print_buffer(t_ant *ant);
void				add_to_buffer(t_ant *ant, char *line);
void				color_code(int i);

#endif
