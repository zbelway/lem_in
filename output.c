/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/18 16:23:21 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/05 14:07:12 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		print_map(t_ant *ant, char **map, int n)
{
	int		i;
	int		j;

	i = -1;
	while (++i < ant->y)
	{
		j = -1;
		while (++j < ant->x)
		{
			if (map[i][j - 1] == ']')
				ft_printf("%{N}");
			if (map[i][j] == '+')
			{
				color_code(n);
				ft_printf("[");
			}
			else
				ft_printf("%c", map[i][j]);
		}
		ft_putchar('\n');
	}
}

int			on_path(t_ant *ant, int i)
{
	int		j;

	j = 0;
	while (++j <= ant->path[i][0] + 1)
	{
		if (ant->farm->room_num == ant->path[i][j])
			return (j % 10);
	}
	return (-1);
}

void		set_up_rooms(t_ant *ant, char **map, int n)
{
	int		l;
	int		i;
	int		j;

	ant->farm = ant->first;
	while (ant->farm->next)
	{
		i = -1;
		l = ft_strlen(ant->farm->room);
		if ((j = on_path(ant, n)) >= 0)
			map[ant->farm->y][ant->farm->x - (l / 2 + 1)] = '+';
		else
			map[ant->farm->y][ant->farm->x - (l / 2 + 1)] = '[';
		if (map[ant->farm->y][ant->farm->x - (l / 2 + 1) - 1] != ' '
				&& (ant->farm->x - (l / 2 + 1)) > 0)
			error(ant, "map too crowded");
		while (++i < l)
			map[ant->farm->y][ant->farm->x - (l / 2) + i] = ant->farm->room[i];
		map[ant->farm->y][ant->farm->x + (l - (l / 2))] = ']';
		if (map[ant->farm->y][ant->farm->x + (l - (l / 2)) + 1] != ' ')
			error(ant, "map too crowded");
		ant->farm = ant->farm->next;
	}
	print_map(ant, map, n);
}

void		set_up_map(t_ant *ant)
{
	char	**map;
	int		i;
	int		j;

	i = -1;
	ant->x += 5;
	ant->y += 5;
	map = (char **)malloc(sizeof(char *) * ant->y);
	while (++i < ant->y)
		map[i] = (char *)malloc(sizeof(char) * ant->x);
	i = -1;
	while (++i < ant->y)
	{
		j = -1;
		while (++j < ant->x)
			map[i][j] = ' ';
	}
	i = -1;
	while (++i < ant->paths)
	{
		color_code(i);
		ft_printf("PATH %d:\n%{N}", i + 1);
		set_up_rooms(ant, map, i);
	}
}
