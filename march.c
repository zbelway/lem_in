/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   march.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 17:59:39 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/04 21:38:20 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	color_code(int i)
{
	if (i > 6)
		i = i % 7;
	if (i == 0)
		ft_printf("%{R}");
	else if (i == 1)
		ft_printf("%{Y}");
	else if (i == 2)
		ft_printf("%{G}");
	else if (i == 3)
		ft_printf("%{C}");
	else if (i == 4)
		ft_printf("%{M}");
	else if (i == 5)
		ft_printf("%{B}");
	else if (i == 6)
		ft_printf("%{W}");
}

void	go_down_paths(t_ant *ant, int *ants, int steps, int paths)
{
	int	i;
	int	moving_ants;
	int	current_step;
	int	ant_path;

	i = -1;
	moving_ants = steps * paths;
	if (moving_ants > ant->ants)
		moving_ants = ant->ants;
	while (++i < moving_ants)
	{
		if (ants[i] != ant->end)
		{
			ant_path = i % paths;
			if (ant->c)
				color_code(i);
			ft_printf("L%d-", i + 1);
			current_step = steps + 1 - (i / paths);
			goto_room_num(ant, ant->path[ant_path][current_step]);
			ft_printf("%s ", ant->farm->room);
			ft_printf("%{N}");
			ants[i] = ant->farm->room_num;
		}
	}
	ft_putchar('\n');
}

void	print_migration(t_ant *ant, int paths)
{
	int	steps;
	int	*ants;

	steps = 0;
	ants = (int *)malloc(sizeof(int) * ant->ants);
	while (steps < ant->ants)
		ants[steps++] = 0;
	if (ant->p)
	{
		steps = -1;
		while (++steps < paths)
			print_path(ant, ant->path, steps);
	}
	steps = 1;
	while (ants[ant->ants - 1] != ant->end)
	{
		go_down_paths(ant, ants, steps, paths);
		steps++;
	}
	if (ant->t)
		ft_printf("Total steps = %d\n", steps - 1);
}

int		check_path(t_ant *ant, int *path)
{
	int	len;

	if (!path)
		return (0);
	len = path[0];
	if (path[1] != ant->start)
		return (0);
	if (path[len + 1] != ant->end)
		return (0);
	return (1);
}

void	first_path(t_ant *ant)
{
	int	len;
	int	paths;

	if (!check_path(ant, ant->path[0]))
	{
		error(ant, "invalid path");
		return ;
	}
	print_buffer(ant);
	ft_putchar('\n');
	paths = 1;
	len = ant->path[0][0];
	while (paths < ant->paths)
	{
		if ((ant->path[paths][0] - len) + paths >= ant->ants)
			break ;
		if (check_path(ant, ant->path[paths]))
			paths++;
		else
			break ;
	}
	print_migration(ant, paths);
}
