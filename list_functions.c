/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/17 16:32:07 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/04 21:29:51 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		create_room(t_ant *ant, char *room, int x, int y)
{
	t_rooms	*new;

	if (!(new = (t_rooms *)malloc(sizeof(t_rooms))))
		error(ant, "creat room malloc");
	ft_bzero(new, sizeof(t_rooms));
	while (ant->farm->next)
		ant->farm = ant->farm->next;
	ant->farm->next = new;
	ant->farm->room = room;
	ant->rooms++;
	ant->farm->room_num = ant->rooms;
	ant->farm->x = x;
	ant->farm->y = y;
	ant->farm->distance = -1;
	if (x > ant->x)
		ant->x = x;
	if (y > ant->y)
		ant->y = y;
}

int			find_room(t_ant *ant, char *room)
{
	ant->farm = ant->first;
	while (ant->farm->next)
	{
		if (ft_strcmp(ant->farm->room, room) == 0)
			return (1);
		ant->farm = ant->farm->next;
	}
	return (0);
}

void		expand_connections(t_ant *ant)
{
	int		i;
	int		*tab;

	i = 0;
	ant->farm->connects_to++;
	tab = (int *)malloc(sizeof(int) * ant->farm->connects_to);
	while (i < ant->farm->connects_to - 1)
	{
		tab[i] = ant->farm->connections[i];
		i++;
	}
	free(ant->farm->connections);
	ant->farm->connections = tab;
}

void		create_connection(t_ant *ant, char *line)
{
	char	**input;
	int		r1;
	int		r2;

	add_to_buffer(ant, line);
	input = ft_strsplit(line, '-');
	find_room(ant, input[0]);
	r1 = ant->farm->room_num;
	expand_connections(ant);
	find_room(ant, input[1]);
	r2 = ant->farm->room_num;
	expand_connections(ant);
	ant->farm->connections[ant->farm->connects_to - 1] = r1;
	find_room(ant, input[0]);
	ant->farm->connections[ant->farm->connects_to - 1] = r2;
}

int			is_connection(t_ant *ant, char *line)
{
	char	**input;

	input = ft_strsplit(line, '-');
	if (input[2])
		return (0);
	if (input[0] && input[1])
	{
		if (find_room(ant, input[0]) && find_room(ant, input[1]))
			return (1);
	}
	free(input);
	return (0);
}
