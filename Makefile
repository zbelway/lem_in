
NAME = lem_in

SRC = main.c \
	  input.c \
	  output.c \
	  list_functions.c \
	  path_distance.c \
	  create_paths.c \
	  march.c \
	  buffer.c \

OBJ = $(subst .c,.o,$(SRC))

FLAGS = -Wall -Werror -Wextra

CC = gcc

RM = rm -rf

LIBS = -L./libft -lft -L./ft_printf -lftprintf

INCLUDES = -I./libft/includes -I./ft_printf/includes

all: $(NAME)

$(NAME): $(OBJ)
	make -C ./ft_printf
	make -C ./libft
	$(CC) $(FLAGS) $(LIBS) $(INCLUDES) -o $(NAME) $(OBJ)

%.o: %.c
	$(CC) $(FLAGS) $(INCLUDES) -c -o$@ $^

clean:
	make clean -C ./ft_printf
	make clean -C ./libft
	$(RM) $(OBJ)

fclean: clean
	make fclean -C ./ft_printf
	make fclean -C ./libft
	$(RM) $(NAME)

re: fclean all clean

.PHONY: re fclean clean all
