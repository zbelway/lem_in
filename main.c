/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/15 22:38:39 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/05 14:08:34 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		error(t_ant *ant, char *line)
{
	if (ant->e)
		ft_printf("Error: %s\n", line);
	else
		ft_printf("Error\n");
	exit(1);
}

void		check_input(t_ant *ant)
{
	if (ant->ants == 0)
		error(ant, "No ants!\n");
	else if (ant->rooms == 0)
		error(ant, "No rooms!");
	else if (ant->start == 0)
		error(ant, "No starting room!");
	else if (ant->end == 0)
		error(ant, "No ending room!");
}

void		bonus_flag(t_ant *ant, char *line)
{
	line++;
	if (*line == 'v')
		ant->v = 'v';
	else if (*line == 'p')
		ant->p = 'p';
	else if (*line == 't')
		ant->t = 't';
	else if (*line == 'c')
		ant->c = 'c';
	else if (*line == 'e')
		ant->e = 'e';
	else
		error(ant, "flag not found.");
	line++;
	while (*line && ft_isspace(*line))
		line++;
	if (*line)
	{
		if (*line == '-')
			bonus_flag(ant, line);
		else
			error(ant, "incorrect flagging.");
	}
}

void		initialize_ant(t_ant *ant)
{
	ant->rooms = 0;
	ant->ants = 0;
	ant->x = 0;
	ant->y = 0;
	ant->paths = 0;
	ant->t = 0;
	ant->p = 0;
	ant->v = 0;
	ant->c = 0;
	ant->e = 0;
	if (!(ant->buf = (char *)malloc(sizeof(char))))
		error(ant, "buffer malloc");
	ant->buf[0] = '\0';
	if (!(ant->farm = (t_rooms *)malloc(sizeof(t_rooms))))
		error(ant, "ant farm malloc");
	ft_bzero(ant->farm, sizeof(t_rooms));
	ant->first = ant->farm;
}

int			main(void)
{
	char	*line;
	t_ant	ant;

	initialize_ant(&ant);
	while (get_next_line(0, &line) > 0)
	{
		if (ant.ants > 0 && ant.rooms > 1 && is_connection(&ant, line))
			create_connection(&ant, line);
		else if (ft_isdigit(*line))
			input_number(&ant, line);
		else if (ant.ants > 0 && *line == '#')
			input_command(&ant, line);
		else if (*line == '-')
			bonus_flag(&ant, line);
		else
			break ;
	}
	check_input(&ant);
	create_paths(&ant);
	first_path(&ant);
	if (ant.v)
		set_up_map(&ant);
	return (0);
}
