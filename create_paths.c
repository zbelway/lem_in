/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_paths.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 18:48:57 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/04 19:51:37 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		erase_path(t_ant *ant, int room_one, int room_two)
{
	int		i;

	i = 0;
	goto_room_num(ant, room_one);
	while (i < ant->farm->connects_to)
	{
		if (ant->farm->connections[i] == room_two)
			ant->farm->connections[i] = 0;
		i++;
	}
	goto_room_num(ant, room_two);
	i = 0;
	while (i < ant->farm->connects_to)
	{
		if (ant->farm->connections[i] == room_one)
			ant->farm->connections[i] = 0;
		i++;
	}
}

void		record_path(t_ant *ant, int **path, int i)
{
	int		len;
	int		j;
	int		d;
	int		room_num;

	len = ant->farm->distance;
	goto_room_num(ant, ant->end);
	path[i] = (int *)malloc(sizeof(int) * (len + 2));
	path[i][0] = len;
	j = len + 1;
	d = len;
	while (j > 0)
	{
		path[i][j] = ant->farm->room_num;
		room_num = ant->farm->room_num;
		if (ant->farm->prev)
			ant->farm = ant->farm->prev;
		erase_path(ant, room_num, ant->farm->room_num);
		j--;
		d--;
	}
}

void		print_path(t_ant *ant, int **path, int i)
{
	int		j;

	j = 1;
	ft_printf("PATH: %d [", i + 1);
	while (j <= path[i][0] + 1)
	{
		goto_room_num(ant, path[i][j]);
		ft_printf("%s", ant->farm->room);
		j++;
		if (j <= path[i][0] + 1)
			ft_printf("->");
	}
	ft_printf("]\n");
}

void		clear_distance(t_ant *ant)
{
	ant->farm = ant->first;
	while (ant->farm->next)
	{
		ant->farm->distance = -1;
		ant->farm->prev = NULL;
		ant->farm = ant->farm->next;
	}
}

void		create_paths(t_ant *ant)
{
	int		i;
	int		paths;
	int		**path;

	i = 0;
	goto_room_num(ant, ant->start);
	paths = ant->farm->connects_to;
	goto_room_num(ant, ant->end);
	if (paths > ant->farm->connects_to)
		paths = ant->farm->connects_to;
	if (paths == 0)
		error(ant, "no paths");
	path = (int **)malloc(sizeof(int *) * paths);
	while (i < paths)
	{
		clear_distance(ant);
		path_distance(ant);
		goto_room_num(ant, ant->end);
		record_path(ant, path, i);
		i++;
	}
	ant->path = path;
	ant->paths = paths;
}
