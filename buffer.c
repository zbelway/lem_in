/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffer.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 19:18:06 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/04 21:52:13 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		print_buffer(t_ant *ant)
{
	int		i;

	i = 0;
	while (ant->buf[i])
		ft_putchar(ant->buf[i++]);
}

void		add_to_buffer(t_ant *ant, char *line)
{
	char	*buf;
	char	*cpy;
	int		i;

	i = 0;
	buf = ft_strnew(ft_strlen(ant->buf) + ft_strlen(line) + 1);
	if (!buf)
		error(ant, "buffer malloc fail");
	cpy = buf;
	while (ant->buf[i])
		*buf++ = ant->buf[i++];
	i = 0;
	while (line[i])
		*buf++ = line[i++];
	*buf++ = '\n';
	*buf = '\0';
	free(ant->buf);
	ant->buf = cpy;
}
