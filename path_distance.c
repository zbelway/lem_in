/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_distance.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 11:36:14 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/04 19:47:12 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		goto_room_num(t_ant *ant, int room_num)
{
	ant->farm = ant->first;
	while (ant->farm->next && ant->farm->room_num != room_num)
		ant->farm = ant->farm->next;
}

int			goto_room_d(t_ant *ant, int d, int i)
{
	goto_room_num(ant, i);
	while (ant->farm->next && ant->farm->distance != d)
		ant->farm = ant->farm->next;
	if (ant->farm->room_num < i)
		return (i);
	else
		return (ant->farm->room_num);
}

void		assign_distance(t_ant *ant, int room_num, int d)
{
	t_rooms	*room;

	room = ant->farm;
	goto_room_num(ant, room_num);
	if (ant->farm->distance >= 0)
		return ;
	ant->farm->prev = room;
	ant->farm->distance = d;
}

void		path_distance(t_ant *ant)
{
	int		i;
	int		j;
	int		d;

	d = 0;
	goto_room_num(ant, ant->start);
	ant->farm->distance = d;
	while (d < ant->rooms)
	{
		i = -1;
		while (++i < ant->rooms)
		{
			j = -1;
			i = goto_room_d(ant, d, i);
			while (++j < ant->farm->connects_to)
			{
				assign_distance(ant, ant->farm->connections[j], d + 1);
				if (ant->farm->room_num == ant->end)
					return ;
				i = goto_room_d(ant, d, i);
			}
		}
		d++;
	}
}
