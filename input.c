/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/15 22:58:02 by zbelway           #+#    #+#             */
/*   Updated: 2016/05/04 22:13:02 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			all_num(char *input)
{
	while (*input && *input != '\n')
	{
		if (ft_isdigit(*input))
			input++;
		else
			return (0);
	}
	return (1);
}

int			parse_number(t_ant *ant, char **input)
{
	if (input[0] && input[1] && input[2])
	{
		if (all_num(input[1]) && all_num(input[2]))
			return (2);
	}
	else if (input[0])
	{
		if (ant->ants == 0)
		{
			if (all_num(input[0]))
				return (1);
		}
	}
	return (0);
}

void		input_number(t_ant *ant, char *line)
{
	char	**input;
	int		parsed;

	add_to_buffer(ant, line);
	input = ft_strsplit(line, ' ');
	parsed = parse_number(ant, input);
	if (ant->ants == 0 && parsed != 1)
		error(ant, "first number must be ants");
	if (parsed == 0)
		error(ant, "not a valid input.");
	else if (parsed == 2)
		create_room(ant, input[0], ft_atoi(input[1]), ft_atoi(input[2]));
	else if (parsed == 1)
		ant->ants = ft_atoi(input[0]);
}

void		input_command(t_ant *ant, char *line)
{
	char	*start_end;
	char	**input;

	start_end = line;
	add_to_buffer(ant, line);
	if (ft_strcmp(line, "##start") == 0 || ft_strcmp(line, "##end") == 0)
	{
		get_next_line(0, &line);
		input = ft_strsplit(line, ' ');
		if (parse_number(ant, input) != 2)
			error(ant, "invalid input");
		add_to_buffer(ant, line);
		create_room(ant, input[0], ft_atoi(input[1]), ft_atoi(input[2]));
		if (ft_strcmp(start_end, "##start") == 0)
			ant->start = ant->farm->room_num;
		else if (ft_strcmp(start_end, "##end") == 0)
			ant->end = ant->farm->room_num;
	}
}
