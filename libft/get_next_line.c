/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 14:28:04 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/14 23:12:09 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

int				read_buf(int fd, char **buf)
{
	int			rd;
	char		input[BUFF_SIZE + 1];
	char		*cpy;

	rd = read(fd, input, BUFF_SIZE);
	if (rd > 0)
	{
		input[rd] = '\0';
		cpy = ft_strjoin(buf[fd], input);
		ft_strdel(&buf[fd]);
		buf[fd] = cpy;
	}
	return (rd);
}

int				get_next_line(int const fd, char **line)
{
	static char	*buf[256];
	char		*endbuf;
	int			rd;

	if (fd < 0 || fd > 256 || BUFF_SIZE < 1 ||
			(!buf[fd] && !(buf[fd] = ft_strnew(1))))
		return (-1);
	endbuf = ft_strchr(buf[fd], END);
	while (!endbuf)
	{
		rd = read_buf(fd, buf);
		if (rd < 0)
			return (-1);
		else if (rd == 0 && (endbuf = ft_strchr(buf[fd], '\0')) == buf[fd])
			return (0);
		else if (rd > 0)
			endbuf = ft_strchr(buf[fd], END);
	}
	if ((*line = ft_strsub(buf[fd], 0, endbuf - buf[fd])) == NULL)
		return (-1);
	if (*endbuf == END)
		endbuf = ft_strdup(endbuf + 1);
	buf[fd] = endbuf;
	return (1);
}
